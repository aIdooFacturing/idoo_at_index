const loadPage = () =>{
	
	$("#container").css({
	//	"background-image" : "linear-gradient(rgb(27,27,31) 50%, #000000)",
		"background" : "black",
		"width" : contentWidth,
		"height" : contentHeight,
		"margin-top" : marginHeight,
		"margin-left" : marginWidth
	})
	
	drawLogoNIcon()
	drawMenuIcon()
}

const drawLogoNIcon = () =>{
	let subTitle = "";
	if(appTy == "auto"){
		subTitle = "(Automotive)"
	}else{
		subTitle = "(Standard)"
	}
	
	const logo = 	
		`
			<img src = "${ctxPath }/images/FL/logo/aidoo_control_v_w.svg"
				style=
					"
						position : absolute
						; z-index : 2
						; width : ${getElSize(184 * 2)}px
						; top : ${getElSize(242 * 2) + marginHeight}px
						; left : ${getElSize(868.5 * 2) + marginWidth}px
					"
			>
		`
	
	const subtitle_text = 
		`
			<span id="subtitle_text"
				style=
					"
						position : absolute
						; z-index : 2
						; color : white
						; font-size : ${getElSize(65)}px
						; top : ${getElSize(410 * 2) + marginHeight}px
					"
			>${subTitle}</span>
		`
		
	const footer_logo = 
		`
			<img src="${ctxPath }/images/FL/default/dmt_logo.svg" id="dmt_logo"
				style=
					"
						position : absolute
						; z-index : 2
						; width : ${getElSize(301.5 * 2)}px
						; height : ${getElSize(33.5 * 2)}px
						; top : ${getElSize(1013.5 * 2) + marginHeight}px
						; left : ${getElSize(809 * 2) + marginWidth}px 
					"
			>
		`
	$("#container").append(logo, footer_logo, subtitle_text)
	
	$("#subtitle_text").css({
		"left" : (originWidth / 2) - ($("#subtitle_text").width() / 2) 
	})
	
}

const drawMenuIcon = () =>{
	let table = "";
	
	if(appTy == "auto"){
		table = 
			`<center>
				<table id="icon_table"
					style=
						"
							 position : absolute
							 ; z-index : 2
							 ; top : ${getElSize(500 * 2) + marginHeight}px
							
						"
					>
					<tr>
						<td><img src="${ctxPath }/images/FL/icon/monitoring_default.svg" ic="monitoring" style="width : ${getElSize(120 * 2)}px"><br><span style="color : white; font-size : ${getElSize(14 * 2)}px">MONITORING</span></td>
						<td><img src="${ctxPath }/images/FL/icon/analysis_default.svg" ic="analysis" style="width : ${getElSize(120 * 2)}px"><br><span style="color : white; font-size : ${getElSize(14 * 2)}px">ANALYSIS</span></td>
						<td><img src="${ctxPath }/images/FL/icon/pm_default.svg" ic="pm" style="width : ${getElSize(120 * 2)}px"><br><span style="color : white; font-size : ${getElSize(14 * 2)}px">PROGRAM</span><br><span style="color : white; font-size : ${getElSize(14 * 2)}px">MANAGEMENT</span></td>
						<td><img src="${ctxPath }/images/FL/icon/kpi_default.svg" ic="kpi" style="width : ${getElSize(120 * 2)}px"><br><span style="color : white; font-size : ${getElSize(14 * 2)}px">KPI</span></td>
						<td><img src="${ctxPath }/images/FL/icon/qm_default.svg" ic="qm" style="width : ${getElSize(120 * 2)}px"><br><span style="color : white; font-size : ${getElSize(14 * 2)}px">QUALITY</span><br><span style="color : white; font-size : ${getElSize(14 * 2)}px">MANAGEMENT</span></td>
						<td><img src="${ctxPath }/images/FL/icon/tm_default.svg" ic="tm" style="width : ${getElSize(120 * 2)}px"><br><span style="color : white; font-size : ${getElSize(14 * 2)}px">TOOL</span><Br><span style="color : white; font-size : ${getElSize(14 * 2)}px">MANAGEMENT</span></td>
						<td><img src="${ctxPath }/images/FL/icon/om_default.svg" ic="om" style="width : ${getElSize(120 * 2)}px"><br><span style="color : white; font-size : ${getElSize(14 * 2)}px">ORDER</span><Br><span style="color : white; font-size : ${getElSize(14 * 2)}px">MANAGEMENT</span></td>
						<td><img src="${ctxPath }/images/FL/icon/maintenance_default.svg" ic="maintenance" style="width : ${getElSize(120 * 2)}px"><br><span style="color : white; font-size : ${getElSize(14 * 2)}px">MAINTENANCE</span></td>
						<td><img src="${ctxPath }/images/FL/icon/config_default.svg" ic="config" style="width : ${getElSize(120 * 2)}px"><br><span style="color : white; font-size : ${getElSize(14 * 2)}px">CONFIGURTION</span></td>
					</tr>
				</table>
			</center>
			`
	}else{
		table = 
			`<center>
				<table id="icon_table"
					style=
						"
							 position : absolute
							 ; z-index : 2
							 ; top : ${getElSize(500 * 2) + marginHeight}px
							
						"
					>
					<tr>
						<td><img src="${ctxPath }/images/FL/icon/monitoring_default.svg" ic="monitoring" style="width : ${getElSize(120 * 2)}px"><br><span style="color : white; font-size : ${getElSize(14 * 2)}px">MONITORING</span></td>
						<td><img src="${ctxPath }/images/FL/icon/analysis_default.svg" ic="analysis" style="width : ${getElSize(120 * 2)}px"><br><span style="color : white; font-size : ${getElSize(14 * 2)}px">ANALYSIS</span></td>
						<td><img src="${ctxPath }/images/FL/icon/maintenance_default.svg" ic="maintenance" style="width : ${getElSize(120 * 2)}px"><br><span style="color : white; font-size : ${getElSize(14 * 2)}px">MAINTENANCE</span></td>
						<td><img src="${ctxPath }/images/FL/icon/config_default.svg" ic="config" style="width : ${getElSize(120 * 2)}px"><br><span style="color : white; font-size : ${getElSize(14 * 2)}px">CONFIGURTION</span></td>
					</tr>
				</table>
			</center>
			`
	}
	
			
	$("#container").append(table)	
	
	
	$("#icon_table td").css({
		"text-align" : "center",
		"padding-left" : getElSize(41.9 / 2),	
		"padding-right" : getElSize(41.9 / 2)
	})
	
	$("#icon_table").css({
		"left" : (originWidth / 2) - ($("#icon_table").width() / 2) 
	})
	
	$("#icon_table img").css({
		"cursor" : "pointer"
	});
	
	bindMyEvt()
}

const bindMyEvt = () =>{
	$("#icon_table td img").not("#main_logo").hover((el)=>{
		const ic = $(el.target).attr("ic")
		
		$(el.target).attr("src", ctxPath + "/images/FL/icon/" + ic + "_pushed.svg")
	}, (el) =>{
		const ic = $(el.target).attr("ic")
		
		$(el.target).attr("src", ctxPath + "/images/FL/icon/" + ic + "_default.svg")
	}).click((el)=>{
		const ic = $(el.target).attr("ic")
		
		let urlTy = "";
		
		if(appTy == "auto"){
			urlTy = ""	
		}else{
			urlTy = "_STD";
		}
			
		if(ic == "monitoring"){
			location.href = `/iDOO${urlTy}_Dashboard/index.do?lang=${lang}` 
		}else if(ic == "analysis"){
			location.href = `/iDOO${urlTy}_Analysis/Performance_Report_Chart.do?lang=${lang}`
		}else if(ic == "pm"){
			location.href = `/iDOO${urlTy}_PM/chart/fileUpDown.do?lang=${lang}`
			//alert("준비 중입니다.")
		}else if(ic == "kpi"){
			location.href = `/iDOO${urlTy}_KPI/kpi/productionStatusKpi_backUp.do?lang=${lang}`
		}else if(ic == "qm"){
			location.href = `/iDOO${urlTy}_QM/chart/checkPrdct.do?lang=${lang}`
		}else if(ic == "tm"){
			location.href = `/iDOO${urlTy}_TM/chart/toolLifeManager.do?lang=${lang}`
		}else if(ic == "om"){
			location.href = `/iDOO${urlTy}_OM/order/addTarget.do?lang=${lang}`
		}else if(ic == "maintenance"){
			location.href = `/iDOO${urlTy}_QM/chart/traceManager.do?lang=${lang}`
		}else if(ic == "config"){
			location.href = `/iDOO${urlTy}_Conf/chart/mstmat.do?lang=${lang}`
			//location.href = `/iDOO${urlTy}_OM/order/addTarget.do`
		}
	})	
}

